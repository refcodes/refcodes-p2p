module org.refcodes.p2p {
	requires transitive org.refcodes.exception;
	requires org.refcodes.mixin;

	exports org.refcodes.p2p;
}
