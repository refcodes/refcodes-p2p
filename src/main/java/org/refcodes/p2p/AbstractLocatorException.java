// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

import org.refcodes.mixin.LocatorAccessor;

/**
 * Base exception for peer ID related exceptions.
 */
public abstract class AbstractLocatorException extends P2PException implements LocatorAccessor<Object> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Object _locator;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aLocator The involved peer locator.
	 */
	public AbstractLocatorException( String aMessage, Object aLocator, Throwable aCause ) {
		super( aMessage, aCause );
		_locator = aLocator;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aLocator The involved peer locator.
	 */
	public AbstractLocatorException( String aMessage, Object aLocator ) {
		super( aMessage );
		_locator = aLocator;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aLocator The involved peer locator.
	 */
	public AbstractLocatorException( Object aLocator, Throwable aCause ) {
		super( aCause );
		_locator = aLocator;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aLocator The involved peer locator.
	 */
	public AbstractLocatorException( String aMessage, Object aLocator, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_locator = aLocator;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aLocator The involved peer locator.
	 */
	public AbstractLocatorException( String aMessage, Object aLocator, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_locator = aLocator;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aLocator The involved peer locator.
	 */
	public AbstractLocatorException( Object aLocator, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_locator = aLocator;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getLocator() {
		return _locator;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getPatternArguments() {
		return new Object[] { _locator };
	}
}
