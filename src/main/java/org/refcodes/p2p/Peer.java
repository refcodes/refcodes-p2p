// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

import java.io.IOException;
import java.util.Set;

/**
 * A {@link Peer} represents a physical peer using {@link PeerRouter} instances
 * to communicate with its direct neighbours. You can think of a {@link Peer}
 * being the skeleton and a {@link PeerRouter} being the stub (as of RPC) of a
 * peer.
 * 
 * @param <ROUTER> The {@link PeerRouter} type to use when routing
 *        {@link P2PMessage} instances.
 * @param <LOCATOR> Defines the type of the locators identifying a peer.
 * @param <HEADER> The {@link P2PHeader} defines the static attributes addressed
 *        for the target of the {@link P2PMessage} (a header might be signed as
 *        it is not modified during dispatch).
 * @param <TAIL> The {@link P2PTail} describes the dynamic attributes required
 *        during dispatch of a {@link P2PMessage} (a tail is modified during
 *        dispatch by having the visited hops appended).
 * @param <MSG> The (sub-)type of the {@link P2PMessage} being processed by the
 *        according {@link Peer} (sub-)type.
 */
public interface Peer<LOCATOR, HEADER extends P2PHeader<LOCATOR>, TAIL extends P2PTail<LOCATOR>, MSG extends P2PMessage<LOCATOR, HEADER, ?, TAIL>, ROUTER extends PeerRouter<LOCATOR, HEADER, TAIL, MSG>> extends PeerRouter<LOCATOR, HEADER, TAIL, MSG>, Terminal<LOCATOR> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves all known (attached) {@link PeerRouter} instances.
	 * 
	 * @return The known (attached) {@link PeerRouter} instances.
	 */
	Set<ROUTER> peerRouters();

	/**
	 * Adds a {@link PeerRouter} to the list of the known {@link PeerRouter}
	 * instances.
	 * 
	 * @param aPeerRouter The {@link PeerRouter} being added.
	 * 
	 * @return False in case this {@link PeerRouter} has already been added.
	 */
	boolean addPeerRouter( ROUTER aPeerRouter );

	/**
	 * Removes a {@link PeerRouter} from the list of the known
	 * {@link PeerRouter} instances.
	 * 
	 * @param aPeerRouter The {@link PeerRouter} to be removed.
	 * 
	 * @return False in case this {@link PeerRouter} has not been added (or
	 *         already been removed).
	 */
	boolean removePeerRouter( ROUTER aPeerRouter );

	/**
	 * Convenience method for {@link #sendMessage(P2PMessage)}.
	 * 
	 * @param <P> The type of the payload in question.
	 * @param aDestination The destination of the message.
	 * @param aPayload The payload to be sent.
	 * 
	 * @throws NoSuchDestinationException thrown in case there is none such
	 *         destination peer.
	 * @throws IOException thrown in case I/O problems occurred while accessing
	 *         the mesh.
	 */
	<P> void sendMessage( LOCATOR aDestination, P aPayload ) throws NoSuchDestinationException, IOException;

}
