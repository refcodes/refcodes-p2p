// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

/**
 * Provides an accessor for a hops property.
 *
 * @param <LOCATOR> The type of the hops to be accessed.
 */
public interface HopsAccessor<LOCATOR> {

	/**
	 * Retrieves the hops from the hops property.
	 * 
	 * @return The hops stored by the hops property.
	 */
	LOCATOR[] getHops();

	/**
	 * Provides a mutator for a hops property.
	 * 
	 * @param <LOCATOR> The type of the hops to be accessed.
	 */
	public interface HopsMutator<LOCATOR> {

		/**
		 * Sets the hops for the hops property.
		 * 
		 * @param aHops The hops to be stored by the hops property.
		 */
		void setHops( LOCATOR[] aHops );
	}

	/**
	 * Provides a builder method for a hops property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HopsBuilder<LOCATOR, B extends HopsBuilder<LOCATOR, B>> {

		/**
		 * Sets the hops for the hops property.
		 * 
		 * @param aHops The hops to be stored by the hops property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHops( LOCATOR[] aHops );
	}

	/**
	 * Provides a hops property.
	 * 
	 * @param <LOCATOR> The type of the hops to be accessed.
	 */
	public interface HopsProperty<LOCATOR> extends HopsAccessor<LOCATOR>, HopsMutator<LOCATOR> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setHops(Object[])}} and returns the very same value (getter).
		 * 
		 * @param aHops The value to set (via {@link #setHops(Object[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default LOCATOR[] letHops( LOCATOR[] aHops ) {
			setHops( aHops );
			return aHops;
		}
	}
}
