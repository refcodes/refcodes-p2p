// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

/**
 * Thrown in case there is none such peer.
 */
public class NoSuchDestinationException extends AbstractLocatorException {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public NoSuchDestinationException( String aMessage, Object aLocator, Throwable aCause ) {
		super( aMessage, aLocator, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public NoSuchDestinationException( String aMessage, Object aLocator ) {
		super( aMessage, aLocator );
	}

	/**
	 * {@inheritDoc}
	 */
	public NoSuchDestinationException( Object aLocator, Throwable aCause ) {
		super( aLocator, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public NoSuchDestinationException( String aMessage, Object aLocator, Throwable aCause, String aErrorCode ) {
		super( aMessage, aLocator, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public NoSuchDestinationException( String aMessage, Object aLocator, String aErrorCode ) {
		super( aMessage, aLocator, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public NoSuchDestinationException( Object aLocator, Throwable aCause, String aErrorCode ) {
		super( aLocator, aCause, aErrorCode );
	}
}
