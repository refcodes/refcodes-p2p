// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

/**
 * Provides an accessor for a hop count property.
 */
public interface HopCountAccessor {

	/**
	 * Retrieves the hop count from the hop count property.
	 * 
	 * @return The hop count stored by the hop count property.
	 */
	int getHopCount();

	/**
	 * Provides a mutator for a hop count property.
	 */
	public interface HopCountMutator {

		/**
		 * Sets the hop count for the hop count property.
		 * 
		 * @param aHopCount The hop count to be stored by the hop count
		 *        property.
		 */
		void setHopCount( int aHopCount );
	}

	/**
	 * Provides a builder method for a hop count property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HopCountBuilder<B extends HopCountBuilder<B>> {

		/**
		 * Sets the hop count for the hop count property.
		 * 
		 * @param aHopCount The hop count to be stored by the hop count
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHopCount( int aHopCount );
	}

	/**
	 * Provides a hop count property.
	 */
	public interface HopCountProperty extends HopCountAccessor, HopCountMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setHopCount(int)} and returns the very same value (getter).
		 * 
		 * @param aHopCount The value to set (via {@link #setHopCount(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letHopCount( int aHopCount ) {
			setHopCount( aHopCount );
			return aHopCount;
		}
	}
}
