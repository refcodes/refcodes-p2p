// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

/**
 * Abstract implementation of a {@link P2PMessage}.
 *
 * @param <LOCATOR> Defines the type of the locators identifying a peer.
 * @param <HEADER> The {@link P2PHeader} defines the static attributes addressed
 *        for the target of the {@link P2PMessage} (a header might be signed as
 *        it is not modified during dispatch).
 * @param <BODY> The type of the payload being carried by the
 *        {@link P2PMessage}.
 * @param <TAIL> The {@link P2PTail} describes the dynamic attributes required
 *        during dispatch of a {@link P2PMessage} (a tail is modified during
 *        dispatch by having the visited hops appended).
 */
public abstract class AbstractP2PMessage<LOCATOR, HEADER extends P2PHeader<LOCATOR>, BODY, TAIL extends P2PTail<LOCATOR>> implements P2PMessage<LOCATOR, HEADER, BODY, TAIL> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected HEADER _header;
	protected BODY _body = null;
	protected TAIL _tail;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link P2PMessage}. No ID trail of already passed
	 * {@link Peer} (or {@link Terminal}) instances is added to this message
	 * (initial message).
	 * 
	 * @param aHeader The {@link P2PHeader} defines the static attributes
	 *        addressed for the target of the {@link P2PMessage} (a header might
	 *        be signed as it is not modified during dispatch).
	 * @param aBody The payload being carried by the {@link P2PMessage}.
	 * @param aTail The {@link P2PTail} describes the dynamic attributes
	 *        required during dispatch of a {@link P2PMessage} (a tail is
	 *        modified during dispatch by having the visited hops appended).
	 */
	protected AbstractP2PMessage( HEADER aHeader, BODY aBody, TAIL aTail ) {
		_header = aHeader;
		_body = aBody;
		_tail = aTail;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HEADER getHeader() {
		return _header;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BODY getBody() {
		return _body;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TAIL getTail() {
		return _tail;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [header=" + _header + ", body=" + _body + ", tail=" + _tail + "]";
	}
}
