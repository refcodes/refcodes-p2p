// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

/**
 * A {@link PeerProxy} represents a proxy for a physical {@link Peer}. A
 * physical {@link Peer} could use serial communication or networking for
 * attaching with another physical {@link Peer} by the abstraction of a
 * {@link PeerProxy}. You can think of a {@link PeerProxy} being the stub and a
 * {@link Peer} being the skeleton (as of RPC) of a peer. As a {@link Peer} is
 * also a {@link PeerProxy}, for testing purposes a P2P network can be
 * established with POJO implementations of a Peer.
 * 
 * @param <LOCATOR> Defines the type of the locators identifying a peer.
 * @param <HEADER> The {@link P2PHeader} defines the static attributes addressed
 *        for the target of the {@link P2PMessage} (a header might be signed as
 *        it is not modified during dispatch).
 * @param <TAIL> The {@link P2PTail} describes the dynamic attributes required
 *        during dispatch of a {@link P2PMessage} (a tail is modified during
 *        dispatch by having the visited hops appended).
 * @param <MSG> The (sub-)type of the {@link P2PMessage} being processed by the
 *        according {@link Peer} (sub-)type.
 */
public interface PeerProxy<LOCATOR, HEADER extends P2PHeader<LOCATOR>, TAIL extends P2PTail<LOCATOR>, MSG extends P2PMessage<LOCATOR, HEADER, ?, TAIL>> extends PeerRouter<LOCATOR, HEADER, TAIL, MSG> {

}
