// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

import org.refcodes.mixin.SourceAccessor;

/**
 * The {@link P2PTail} describes the dynamic attributes required during dispatch
 * of a {@link P2PMessage} (a tail is modified during dispatch by having the
 * visited hops appended).
 * 
 * @param <LOCATOR> Defines the type of the locators identifying a peer.
 */
public interface P2PTail<LOCATOR> extends SourceAccessor<LOCATOR>, HopsAccessor<LOCATOR> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the trail of peers already passed.
	 * 
	 * @return The trail of peers already passed (in order to avoid turning
	 *         circles when determining the hop count).
	 */
	@Override
	LOCATOR[] getHops();

	/**
	 * Adds a stop-over peer, being the peer which received the message for
	 * dispatching to the next peer. The stop-over will be prepended to the
	 * trail of peers.
	 * 
	 * @param aLocator the stop-over peer while dispatching the message.
	 */
	void appendHop( LOCATOR aLocator );

	/**
	 * Tests whether the providing {@link P2PMessage} has already stopped over
	 * the given {@link Peer}.
	 * 
	 * @param aLocator The locator of the {@link Peer} in question.
	 * 
	 * @return True in case the according {@link P2PMessage} has already stopped
	 *         over the {@link Peer} in question.
	 */
	default boolean containsHop( LOCATOR aLocator ) {
		for ( int i = 0; i < getHops().length; i++ ) {
			if ( getHops()[i].equals( aLocator ) ) {
				return true;
			}
		}
		return false;
	}
}
