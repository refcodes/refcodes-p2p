// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

/**
 * A message consumer is a functional interfaces consuming messages.
 * 
 * @param <MSG> The (sub-)type of the {@link P2PMessage} being processed by the
 *        according {@link Peer} (sub-)type.
 */
@FunctionalInterface
public interface P2PMessageConsumer<MSG extends P2PMessage<?, ?, ?, ?>, PEER extends Peer<?, ?, ?, MSG, ?>> {

	/**
	 * Invoked when a message is being consumed.
	 * 
	 * @param aMessage The message to be consumed.
	 * @param aPeer The {@link Peer} notifying upon the message.
	 */
	void onP2PMessage( MSG aMessage, PEER aPeer );
}
