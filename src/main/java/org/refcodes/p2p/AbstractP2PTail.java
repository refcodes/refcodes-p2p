// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Abstract implementation of a {@link P2PTail}.
 * 
 * @param <LOCATOR> Defines the type of the locators identifying a peer.
 */
public class AbstractP2PTail<LOCATOR> implements P2PTail<LOCATOR> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected LOCATOR[] _hops = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the tail. The source is added upon transmission as first
	 * stop-over.
	 */
	public AbstractP2PTail() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LOCATOR getSource() {
		return _hops != null && _hops.length != 0 ? _hops[0] : null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LOCATOR[] getHops() {
		return _hops;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void appendHop( LOCATOR aLocator ) {
		if ( _hops == null ) {
			_hops = (LOCATOR[]) Array.newInstance( aLocator.getClass(), 0 );
		}
		final LOCATOR[] theTrail = (LOCATOR[]) Array.newInstance( _hops.getClass().getComponentType(), _hops.length + 1 );
		for ( int i = 0; i < _hops.length; i++ ) {
			theTrail[i] = _hops[i];
		}
		theTrail[theTrail.length - 1] = aLocator;
		_hops = theTrail;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [trail=" + Arrays.toString( _hops ) + "]";
	}
}
