// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

import java.io.IOException;
import java.lang.reflect.Array;

/**
 * A {@link PeerRouter} defines functionality to route a {@link P2PMessage}.
 * 
 * @param <LOCATOR> Defines the type of the locators identifying a peer.
 * @param <HEADER> The {@link P2PHeader} defines the static attributes addressed
 *        for the target of the {@link P2PMessage} (a header might be signed as
 *        it is not modified during dispatch).
 * @param <TAIL> The {@link P2PTail} describes the dynamic attributes required
 *        during dispatch of a {@link P2PMessage} (a tail is modified during
 *        dispatch by having the visited hops appended).
 * @param <MSG> The (sub-)type of the {@link P2PMessage} being processed by the
 *        according {@link Peer} (sub-)type.
 */
public interface PeerRouter<LOCATOR, HEADER extends P2PHeader<LOCATOR>, TAIL extends P2PTail<LOCATOR>, MSG extends P2PMessage<LOCATOR, HEADER, ?, TAIL>> { // extends Terminal<LOCATOR> { // "extends" Not required for routing!

	/**
	 * Determines the (nearest) hop count to the given peer.
	 * 
	 * @param aDestination The unique ID of the peer for which to get the hop
	 *        count.
	 * 
	 * @return The according hop count or -1 if the targeted peer is
	 *         unreachable.
	 * 
	 * @throws IOException thrown in case I/O problems occurred while accessing
	 *         the mesh.
	 */
	@SuppressWarnings("unchecked")
	default int getHopCount( LOCATOR aDestination ) throws IOException {
		return getHopCount( aDestination, (LOCATOR[]) Array.newInstance( aDestination.getClass(), 0 ) );
	}

	/**
	 * Determines the (nearest) hop count to the given peer.
	 * 
	 * @param aLocator The unique ID of the peer for which to get the hop count.
	 * @param aHops The trail of peers already passed (in order to avoid turning
	 *        circles when determining the hop count).
	 * 
	 * @return The according hop count or -1 if the targeted peer is
	 *         unreachable.
	 * 
	 * @throws IOException thrown in case I/O problems occurred while accessing
	 *         the mesh.
	 */
	int getHopCount( LOCATOR aLocator, LOCATOR[] aHops ) throws IOException;

	/**
	 * Passes a message to the given peer.
	 * 
	 * @param aMessage The message to be passed.
	 * 
	 * @throws NoSuchDestinationException thrown in case there is none such
	 *         destination peer.
	 * @throws IOException thrown in case I/O problems occurred while accessing
	 *         the mesh.
	 */
	void sendMessage( MSG aMessage ) throws NoSuchDestinationException, IOException;

}
