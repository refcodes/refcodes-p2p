// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p;

import org.refcodes.exception.UnmarshalException;
import org.refcodes.mixin.Message;
import org.refcodes.mixin.TailAccessor;

/**
 * A {@link P2PMessage} is a transient passing various peers till reaching its
 * target, consisting of a target, a payload and a trail of visited (peer)
 * nodes. For routing by stateless {@link Peer} instances, a {@link P2PMessage}
 * carries a trail of visited nodes to prevent turning circles through the
 * {@link Peer} instances.
 * 
 * @param <LOCATOR> Defines the type of the locators identifying a peer.
 * @param <HEADER> The {@link P2PHeader} defines the static attributes addressed
 *        for the target of the {@link P2PMessage} (a header might be signed as
 *        it is not modified during dispatch).
 * @param <BODY> The type of the payload being carried by the
 *        {@link P2PMessage}.
 * @param <TAIL> The {@link P2PTail} describes the dynamic attributes required
 *        during dispatch of a {@link P2PMessage} (a tail is modified during
 *        dispatch by having the visited hops appended).
 */
public interface P2PMessage<LOCATOR, HEADER extends P2PHeader<LOCATOR>, BODY, TAIL extends P2PTail<LOCATOR>> extends Message<HEADER, BODY>, TailAccessor<TAIL> {

	/**
	 * Retrieves the object representing the message's payload.
	 *
	 * @param <P> the type of the payload
	 * @param aResponseType The type of which the response is expected to be.
	 * 
	 * @return An instance of the type representing the response.
	 * 
	 * @throws UnmarshalException thrown when unmarshaling / deserializing an
	 *         object fails.
	 */
	<P> P getPayload( Class<P> aResponseType ) throws UnmarshalException;

}
